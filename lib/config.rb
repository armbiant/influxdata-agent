require 'influxdb'

user = ENV.fetch('INFLUXDB_USER')
pass = ENV.fetch('INFLUXDB_PASSWORD')

CLIENT = InfluxDB::Client.new(
  host:     'yorick-influxdb.gitlap.com',
  database: 'gitlab',
  username: user,
  password: pass
)
