# Inspector Influx

A collection of Ruby scripts to pull data out of GitLab's InfluxDB database to
ease the process of finding (potential) performance problems.

## Requirements

* Ruby
* InfluxDB credentials

## Usage

Install the huge list of dependencies:

    bundle install

Run a script of choice:

    INFLUXDB_USER=alice INFLUXDB_PASSWORD=hunter2 ruby lib/SCRIPT.rb

## License

All source code in this repository is subject to the terms of the MIT license,
unless stated otherwise. A copy of this license can be found the file "LICENSE".

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md).
